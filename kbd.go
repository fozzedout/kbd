package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"time"
	"unsafe"
)

const (
	BATTERYPATH = "/sys/class/power_supply/axp20x-battery/capacity"
	// Switch to ../constant_charge_current to allow overcurrent for devices
	CHARGEPATH     = "/sys/class/power_supply/axp20x-usb/input_current_limit"
	BATTERYPATHPPP = "/sys/class/power_supply/rk818-battery/capacity"
	CHARGEPATHPPP  = "/sys/class/power_supply/rk818-usb/input_current_limit"
	I2cCheckPath   = "/sys/class/i2c-adapter/i2c-@@i@@/uevent"
	I2cPath        = "/dev/i2c-@@i@@"
	// Info registers from Megi's userspace charger controller
	READ0                uint8  = 0x71
	READ1                uint8  = 0x72
	READ2                uint8  = 0x77
	RegSysChargeAddr     uint8  = 0x21
	RegSysChgData        uint8  = 0x22
	RegSysCommandChgRead uint8  = 0x91
	BufConstant          uint8  = 0xAA
	KBAddr               uint16 = 0x15
	BatVADCDATL          uint8  = 0xa2
	BatVADCDATH          uint8  = 0xa3
	// Taken from uapi/linux/i2c-dev.h
	I2CRDWR uintptr = 0x0707
	I2CMRD  uintptr = 0x0001
	// Errors returned by I2C IOCTL
	ErrNoSuchDevice = "no such device or address"
	ErrNoConnect    = "Could not connect to I2C device"
	waitTime        = 20 * time.Second
	// gsettings stuff
	DBUSADDRESS = "DBUS_SESSION_BUSS_ADDRESS=unix:path=/run/user/1000/bus"
	XDGRUNTIME  = "XDG_RUNTIME_DIR=/run/user/1000/"
	SCHEMADIR   = "org.gnome.desktop.a11y.applications"
	SCHEMANAME  = "screen-keyboard-enabled"
)

// Errors
var (
	I2CProxyErorr = errors.New("Could not connect to I2C device")
)

type i2c_msg struct {
	addr      uint16
	flags     uint16
	len       uint16
	__padding uint16
	buf       uintptr
}

type i2c_rdwr_ioctl_data struct {
	msgs  uintptr
	nmsgs uint32
}

func open_i2c() (*os.File, error) {
	path := ""
	for i := 0; i < 8; i++ {
		path = strings.Replace(I2cCheckPath, "@@i@@", strconv.Itoa(i), 1)
		content, err := os.ReadFile(path)
		if err != nil {
			fmt.Printf("Tried opening %s and failed with %+v\n", path, err)
			continue
		}
		if strings.Contains(string(content), "OF_FULLNAME=/soc/i2c@1c2b400") {
			path = strings.Replace(I2cPath, "@@i@@", strconv.Itoa(i), 1)
			break
		}

	}
	file, err := os.OpenFile(path, os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func read_power(i2cfile *os.File, register uint8) (uint8, error) {
	buf := []uint8{RegSysChargeAddr, register, BufConstant, RegSysCommandChgRead}

	msgs := []i2c_msg{
		{
			addr:  KBAddr,
			flags: 0,
			len:   4,
			buf:   uintptr(unsafe.Pointer(&buf[0])),
		},
	}
	msg := i2c_rdwr_ioctl_data{
		msgs:  uintptr(unsafe.Pointer(&msgs[0])),
		nmsgs: 1,
	}
	_, _, errno := syscall.Syscall(syscall.SYS_IOCTL, uintptr(i2cfile.Fd()), I2CRDWR, uintptr(unsafe.Pointer(&msg)))
	if errno != 0 {
		// fmt.Printf("Error conducting IOCTL syscall: %v - %s\n", errno, errno.Error())
		return 0, errno
	}

	for i := 0; i < 5; i++ {
		time.Sleep(700 * time.Microsecond)
		buf2 := []uint8{RegSysChgData}
		rdbuf := []uint8{0, 0}
		msgs2 := []i2c_msg{
			{
				addr:  KBAddr,
				flags: 0,
				len:   1,
				buf:   uintptr(unsafe.Pointer(&buf2[0])),
			},
			{
				addr:  KBAddr,
				flags: uint16(I2CMRD),
				len:   uint16(len(rdbuf)),
				buf:   uintptr(unsafe.Pointer(&rdbuf[0])),
			},
		}
		msg2 := i2c_rdwr_ioctl_data{
			msgs:  uintptr(unsafe.Pointer(&msgs2[0])),
			nmsgs: 2,
		}
		_, _, errno := syscall.Syscall(syscall.SYS_IOCTL, uintptr(i2cfile.Fd()), I2CRDWR, uintptr(unsafe.Pointer(&msg2)))
		if errno != 0 {
			// fmt.Printf("Error conducting IOCTL syscall: %v - %s\n", errno, errno.Error())
			return 0, errno
		}
		if rdbuf[1] == RegSysCommandChgRead {
			continue
		}
		if rdbuf[1] == 0 {
			return rdbuf[0], nil
		}
		return 0, I2CProxyErorr
	}
	return 0, I2CProxyErorr
}

func getVoltage(i2cFile *os.File) (uint, error) {
	l, err := read_power(i2cFile, BatVADCDATL)
	if err != nil {
		return 0, err
	}
	h, err := read_power(i2cFile, BatVADCDATH)
	if err != nil {
		return 0, err
	}

	high := uint(h)
	low := uint(l)
	if h&0x20 != 0 {
		return 2600 - (uint(^l&0xff)+((uint(^high)&0x1f)<<8)+1)*1000/3724, nil
	}
	return 2600 + ((low+(high<<8))*1000)/3724, nil

}

func main() {
	i2cFile, err := open_i2c()
	if err != nil {
		fmt.Printf("Error opening I2C file: %+v\n", err)
	}
	gsettingsKbdOnCmd := exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "true")
	gsettingsKbdOnCmd.Run()
	gsettingsKbdOn := true
	for {
		kbdVoltage, err := getVoltage(i2cFile)
		if err != nil {
			fmt.Printf("got error: %+v\n", err)
			switch err.Error() {
			case ErrNoSuchDevice:
				// Kebyoard not attached
				if !gsettingsKbdOn {
					gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "true")
					err = gsettingsKbdOnCmd.Run()
					if err != nil {
						fmt.Printf("Could not set keyboard on: %+v\n", err)
						out, _ := gsettingsKbdOnCmd.Output()
						fmt.Printf("Output from gsettings: %s", string(out))
					}
					gsettingsKbdOn = true
				}
				time.Sleep(waitTime)
				continue
			case ErrNoConnect:
				// Keyboard attached but no battery
				if gsettingsKbdOn {
					gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "false")
					err = gsettingsKbdOnCmd.Run()
					if err != nil {
						fmt.Printf("Could not set keyboard on: %+v\n", err)
						out, _ := gsettingsKbdOnCmd.Output()
						fmt.Printf("Output from gsettings: %s", string(out))
					}
					gsettingsKbdOn = false
				}
				time.Sleep(waitTime)
				continue
			default:
				fmt.Printf("Unexpected error: %+v\n", err)
				time.Sleep(waitTime)
				continue
			}
		}
		if gsettingsKbdOn {
			gsettingsKbdOnCmd = exec.Command("sudo", "-u#1000", DBUSADDRESS, XDGRUNTIME, "gsettings", "set", SCHEMADIR, SCHEMANAME, "false")
			err = gsettingsKbdOnCmd.Run()
			if err != nil {
				fmt.Printf("Could not set keyboard off: %+v\n", err)
				out, _ := gsettingsKbdOnCmd.Output()
				fmt.Printf("Output from gsettings: %s", string(out))
			}
			gsettingsKbdOn = false
		}
		content, err := os.ReadFile(BATTERYPATH)
		if err != nil {
			fmt.Printf("Error reading %s: %+v\n", BATTERYPATH, err)
			time.Sleep(waitTime)
			continue
		}
		capacity, err := strconv.Atoi(strings.TrimSpace(string(content)))
		if err != nil {
			fmt.Printf("Error parsing battery capacity %s: %s\n", string(content), err)
			time.Sleep(waitTime)
			continue
		}
		content, err = os.ReadFile(CHARGEPATH)
		if err != nil {
			fmt.Printf("Error reading %s: %+v\n", CHARGEPATH, err)
			time.Sleep(waitTime)
			continue
		}
		inputCurrent, err := strconv.Atoi(strings.TrimSpace(string(content)))
		if err != nil {
			fmt.Printf("Error parsing input current %s: %s\n", string(content), err)
			time.Sleep(waitTime)
			continue
		}

		expectedCurrent := 500000
		switch {
		case kbdVoltage < 3200:
			expectedCurrent = 500000
		case capacity < 80:
			expectedCurrent = 1500000
		case capacity < 90:
			expectedCurrent = 900000
		}

		if inputCurrent != expectedCurrent {
			fmt.Printf("Capacity is %v, input is %v, setting input to %v\n", capacity, inputCurrent, expectedCurrent)
			send := strconv.Itoa(expectedCurrent)
			limitFile, err := os.OpenFile(CHARGEPATH, os.O_RDWR, 0644)
			if err != nil {
				fmt.Printf("Error opening %s, %+v\n", CHARGEPATH, err)
				time.Sleep(waitTime)
				continue
			}
			_, err = limitFile.WriteString(send)
			if err != nil {
				fmt.Printf("Error writing to  %s, %+v\n", CHARGEPATH, err)
				time.Sleep(waitTime)
				continue
			}
			err = limitFile.Sync()
			if err != nil {
				fmt.Printf("Error syncing %s, %+v\n", CHARGEPATH, err)
				time.Sleep(waitTime)
				continue
			}
		}
		time.Sleep(waitTime)
	}

}
